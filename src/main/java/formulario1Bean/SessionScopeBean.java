package formulario1Bean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

@ManagedBean
@RequestScoped
public class SessionScopeBean implements Serializable{
	private static final long serialVersionUID = 1299252626962880183L;

	
private String valor1, valor2;


public String getValor1() {
	return valor1;
}


public void setValor1(String valor1) {
	this.valor1 = valor1;
}


public String getValor2() {
	return valor2;
}


public void setValor2(String valor2) {
	this.valor2 = valor2;
}


@Override
public String toString() {
	return "SessionScopeBean [valor1=" + valor1 + ", valor2=" + valor2 + "]";
}


}
